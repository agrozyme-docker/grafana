# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Grafana
      module Build
        def self.main
          System.run('apk add --no-cache grafana')
        end
      end

      module Run
        def self.main
          Shell.update_user
          System.execute('/usr/sbin/grafana-server', {}, '')
        end

      end
    end
  end
end
